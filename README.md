# Code project for flitering and displaying products 

This project was bootstrapped with [TypeScript React Starter App](https://github.com/Microsoft/TypeScript-React-Starter). It uses React.js, Typescript and written in ES6+ syntax, forked from Create-React-App.

## Installing / Getting started

To get started just use "npm"

## Developing

### Built With
 Babel, Typescript, Jest, SaSS, TSLint, Prettier


## Usage
> NPM + Node.js CLI tool required

```shell
git clone https://bitbucket.org/alonfai/testproject
cd testProject/
npm install
npm start
```

## Tests

All tests are written in*.spec.js files. To run all just run:

```shell
npm test
```

This also will create code coverage output. It uses instanbul command (store as "html" report under coverage/index.html folder)

> You can also run the tests in --watch mode with

```shell
npm run test:watch
```

## Build

To build the project assets for prodcuction use and deployment to a static remote web server, run the following command:

```shell
npm run build
```

> It will create the minified hashed resources (.js, .css, .jpg) along with the source-map and mainfests files under build/* folder

## Style

Uses TSLint.json rules


## Licensing

MIT License
