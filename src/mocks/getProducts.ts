import { Product } from 'interfaces';

/**
 * get mocked data for Product id "1"
 */
export function getProductOne(): Product {
  return {
    index: 0,
    isExclusive: false,
    isSale: false,
    price: '$15.35',
    productImage: 'http://localhost/productOne.jpg',
    productName: 'Product One',
    size: ['XL', 'M']
  };
}

/**
 * get mocked data for Product id "2"
 */
export function getProductTwo(): Product {
  return {
    index: 1,
    isExclusive: false,
    isSale: false,
    price: '$10.15',
    productImage: 'http://localhost/productTwo.jpg',
    productName: 'Product One',
    size: ['XS', 'S']
  };
}

/**
 * get mocked data for Product id "3"
 */
export function getProductThree(): Product {
  return {
    index: 1,
    isExclusive: false,
    isSale: false,
    price: '$50.5',
    productImage: 'http://localhost/productThree.jpg',
    productName: 'Product Three',
    size: ['XL', 'L']
  };
}
