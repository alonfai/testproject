declare module '*.scss' {
  const styles: { [className: string]: string };
  export = styles;
}

declare module '*.json' {
  const json: any;
  export = json;
}
