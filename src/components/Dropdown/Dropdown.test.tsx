import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { DEFAULT_SIZE } from 'utils';
import Dropdown, { ComponentProps } from './Dropdown';

describe('<Dropdown />', () => {
  const onChangeMock = jest.fn();
  const defaultProps: ComponentProps = {
    onChange: onChangeMock,
    selectedSise: DEFAULT_SIZE
  };
  let wrapper: ShallowWrapper<ComponentProps>;

  const setup = (props = defaultProps) => {
    return shallow(<Dropdown {...props} />);
  };

  beforeEach(() => {
    wrapper = setup();
  });

  it('should render successfully', () => {
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toBeTruthy();
  });
});
