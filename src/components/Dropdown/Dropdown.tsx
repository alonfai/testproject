import React from 'react';
import Select from 'react-select';
import { Option } from 'interfaces';
import { filterOptions, DEFAULT_SIZE } from 'utils';

export interface ComponentProps {
  onChange: (selectedSize: Option) => void;
  selectedSise: Option;
  className?: string;
}

const Dropdown: React.SFC<ComponentProps> = ({ onChange, selectedSise, className }) => {
  return <Select value={selectedSise} onChange={onChange} options={filterOptions} className={className} />;
};

export default Dropdown;
