import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { Product } from 'interfaces';
import { getProductOne } from 'mocks/getProducts';
import ProductItem, { ComponentProps } from './ProductItem';

describe('<ProductItem />', () => {
  const product: Product = getProductOne();

  const defaultProps: ComponentProps = {
    product
  };
  let wrapper: ShallowWrapper<ComponentProps>;

  const setup = (props = defaultProps) => {
    return shallow(<ProductItem {...props} />);
  };

  beforeEach(() => {
    wrapper = setup();
  });

  it('should render successfully', () => {
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toBeTruthy();
  });

  it('check it has all attributes', () => {
    expect(wrapper.find('img').length).toEqual(1);
    expect(wrapper.find('.Container__Details').exists()).toEqual(true);
  });

  it('check exclusive icon is shown when the flag of isExclusive = true', () => {
    const props: ComponentProps = {
      ...defaultProps,
      product: {
        ...defaultProps.product,
        isExclusive: true
      }
    };
    wrapper = setup(props);
    expect(wrapper).toBeTruthy();
    expect(wrapper.find('.Container__Exclusive').length).toEqual(1);
  });

  it('check sale icon is shown when the flag of isSale = true', () => {
    const props: ComponentProps = {
        ...defaultProps,
        product: {
          ...defaultProps.product,
          isSale: true
        }
      };
      wrapper = setup(props);
      expect(wrapper).toBeTruthy();
      expect(wrapper.find('.Container__Sale').length).toEqual(1);
  });
});
