import React from 'react';
import { Product } from 'interfaces';

import styles from './ProductItem.scss';

export interface ComponentProps {
  /**
   * given product item
   */
  product: Product;
}

const ProductItem: React.SFC<ComponentProps> = ({ product }) => {
  const url = require(`../../assets/${product.productImage}`);
  return (
    <div className={styles.Container}>
      <img src={url} alt={product.productName} />
      {product.isExclusive && <div className={styles.Container__Exclusive}>Exclusive</div>}
      {product.isSale && <div className={styles.Container__Sale}>Sale</div>}
      <div className={styles.Container__Details}>
        <span>{product.productName}</span>
        <span>{product.price}</span>
      </div>
    </div>
  );
};

export default ProductItem;
