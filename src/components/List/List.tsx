import React from 'react';
import cx from 'classnames';
import { Product } from 'interfaces';
import { Option } from 'interfaces';
import ProductItem from '../ProductItem';
import { getProductsBySize } from 'utils';

import styles from './List.scss';

export interface ComponentProps {
  selectedSize?: Option;
  className?: string;
}

const List: React.SFC<ComponentProps> = ({ selectedSize, className }) => {
  const products: Product[] = getProductsBySize(selectedSize);
  const prefix = 'Product_';
  return (
    <div className={cx({ [styles.Container]: true, [className]: !!className })}>
      {products.map(product => (
        <ProductItem key={`${prefix}${product.index}`} product={product} />
      ))}
    </div>
  );
};

export default List;
