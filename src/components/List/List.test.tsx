import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { DEFAULT_SIZE } from 'utils';
import List, { ComponentProps } from './List';

describe('<List />', () => {
  const defaultProps: ComponentProps = {
    selectedSize: DEFAULT_SIZE
  };
  let wrapper: ShallowWrapper<ComponentProps>;

  const setup = (props = defaultProps) => {
    return shallow(<List {...props} />);
  };

  beforeEach(() => {
    wrapper = setup();
  });

  it('should render successfully', () => {
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toBeTruthy();
  });

  it('should render <ProductItem /> components', () => {
    expect(wrapper.find('ProductItem').length).toBeGreaterThanOrEqual(1);
  });
});
