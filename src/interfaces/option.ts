/**
 * interface for defining the given attributes for any product size filter option
 */
export default interface Option {
  /**
   * option text shown in drop down of filters
   */
  label: string;
  /**
   * option value
   */
  value?: string;
  /**
   * (Optional) custom wrapper className for the component
   */
  className?: string;
}
