/**
 * interface for defining the different attributes of any given product
 */
export default interface Product {
  /**
   * 0-based index of the product in the list
   */
  index: number;
  /**
   * does the product is on sale
   */
  isSale: boolean;
  /**
   * does the product is exclusive sale
   */
  isExclusive: boolean;
  /**
   * value price in "$" for the product
   */
  price: string;
  /**
   * URL based for the product image
   */
  productImage: string;
  /**
   * name of product
   */
  productName: string;
  /**
   * different size options available for the shirt
   */
  size: string[];
}
