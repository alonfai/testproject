import getProductsBySize from './getProductsBySize';
import { DEFAULT_SIZE } from './options';
import { Option, Product } from 'interfaces';
import products from 'assets/products.json';

describe('utils/getProductsBySize', () => {
  it('check if provided size is undefined/null', () => {
    expect(getProductsBySize(DEFAULT_SIZE)).toEqual(products);
    expect(getProductsBySize(undefined)).toEqual(products);
  });

  it('check if provided size is the "DEFAULT_SIZE" option returns the full list', () => {
    expect(getProductsBySize(DEFAULT_SIZE)).toEqual(products);
  });

  it('check that a sub list of products is returned when provided size exists', () => {
    const size: string = (products[0] as Product).size[0];
    const option: Option = {
      label: size,
      value: size
    };
    const expectedProducts = (products as Product[]).filter(p => p.size.includes(size));
    expect(getProductsBySize(option)).toEqual(expectedProducts);
  });
});
