import { uniq } from 'lodash';
import { Product, Option } from 'interfaces';

import data from 'assets/products.json';

/**
 * retrieve the list of all sizes available from the list of products.
 * Note: This uses the reduce functionality supported on arrays in JavaScript
 */
export default function getSizes(): Option[] {
  const products: Product[] = data;
  const sizes = products.map(p => p.size).reduce((accumulator, currentValue) => {
    return accumulator.concat(currentValue);
  });
  return uniq(sizes).map(size => ({
    label: size,
    value: size
  }));
}
