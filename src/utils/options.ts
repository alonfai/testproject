import { Option } from 'interfaces';
import getSizes from './getSizes';

/**
 * Default filter size option
 */
export const DEFAULT_SIZE: Option = {
  label: 'Filter by size'
};

/**
 * list of all possible filters for the sizes
 */
export const filterOptions: Option[] = [DEFAULT_SIZE, ...getSizes()];
