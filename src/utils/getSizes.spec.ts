import getSizes from './getSizes';

describe('utils/getSizes', () => {
  it('check the list is unique with no duplicates', () => {
    const list = getSizes();
    const found = list.find((element, index) => list.indexOf(element) !== index);
    expect(found).toBeUndefined();
  });
});
