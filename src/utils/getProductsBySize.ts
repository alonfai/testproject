import { isEqual } from 'lodash'
import { Product, Option } from 'interfaces';
import { DEFAULT_SIZE } from './options';

import products from 'assets/products.json';

/**
 * retrieve the list of product items in the app
 */
export default function getProductsBySize(selectedSize: Option = DEFAULT_SIZE): Product[] {
  return !isEqual(selectedSize, DEFAULT_SIZE) ? products.filter(product => product.size.includes(selectedSize.value)) : products;
}
