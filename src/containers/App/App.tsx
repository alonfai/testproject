import React from 'react';
import { hot } from 'react-hot-loader'
import Dropdown from 'components/Dropdown';
import { DEFAULT_SIZE } from 'utils';
import { Option } from 'interfaces';
import List from 'components/List';

import styles from './App.scss';

export interface ComponentProps {}
export interface ComponentState {
  /**
   * user selected shirt size to filter
   */
  selectedSize?: Option;
}

export class App extends React.Component<ComponentProps, ComponentState> {
  constructor(props: ComponentProps) {
    super(props);
    this.state = {
      selectedSize: DEFAULT_SIZE
    };
  }

  onChangeSize = (selectedSize: Option) => {
    this.setState({
      selectedSize
    });
  };

  render() {
    const { selectedSize } = this.state;
    return (
      <div className={styles.Container}>
        <div className={styles.Title}>
          <div className={styles.Title__Label}>Women's tops</div>
          <Dropdown onChange={this.onChangeSize} selectedSise={selectedSize} className={styles.Title__Dropdown} />
        </div>
        <List selectedSize={selectedSize} className={styles.List} />
      </div>
    );
  }
}

export default hot(module)(App);
