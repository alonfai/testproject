import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { App, ComponentProps, ComponentState } from './App';
import { Option } from 'interfaces';

describe('<App />', () => {
  const defaultProps: ComponentProps = {};
  let wrapper: ShallowWrapper<ComponentProps, ComponentState>;

  const setup = (props = defaultProps) => {
    return shallow(<App {...props} />);
  };

  beforeEach(() => {
    wrapper = setup();
  });

  it('should render successfully', () => {
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toBeTruthy();
  });

  it('should contain <Dropdown /> and <List /> component', () => {
    expect(wrapper.find('Dropdown').exists()).toEqual(true);
    expect(wrapper.find('List').exists()).toEqual(true);
  });

  it('test the onChangeSize event change the state', () => {
    const newOption: Option = {
      label: 'Option 1',
      value: '1'
    };
    const instance = wrapper.instance() as App;
    instance.onChangeSize(newOption);
    expect(wrapper.state().selectedSize).toEqual(newOption);
  });
});
